let fs = require('fs');

class Pizza {
  constructor() {
    this.emptyDot = {
      x: undefined,
      y: undefined,
      value: undefined,
    };
  }

  cut(inputFilePath, outputFilePath) {
    let fileContent = fs.readFileSync(inputFilePath).toString();
    let originalPizzaData = this.readData(fileContent);
    let resultSlices;
    let resultIngredientsSum = 0;
    this.getPermutation(['bottom', 'top', 'right', 'left']).forEach(
      directions => {
        let slices = [];
        let pizzaData = this.getCopy(originalPizzaData);
        let useSlicesMerge = false;
        do {
          this.rerunJoinLoop = false;
          this.joinLoop({
            slices,
            pizzaData,
            useSlicesMerge,
            directions
          });
          if (!this.rerunJoinLoop && !useSlicesMerge) {
            this.rerunJoinLoop = true;
            useSlicesMerge = true;
          }
        } while (this.rerunJoinLoop);
        let sum = 0;
        let completedSlices = slices.filter(slice => this.isSliceTotalCompleted(slice.total));
        completedSlices.forEach(slice => {
          sum += slice.total.T + slice.total.M;
        });
        if (sum > resultIngredientsSum) {
          resultIngredientsSum = sum;
          resultSlices = completedSlices;
        }
      }
    )
    console.log(`find`, resultIngredientsSum);
    fs.writeFileSync(outputFilePath, this.getOutputData(resultSlices));
  }

  getPermutation(array) {
    let p = (array, temp) => {
      let i, x;
      if (!array.length) {
        result.push(temp);
      }
      for (i = 0; i < array.length; i++) {
        x = array.splice(i, 1)[0];
        p(array, temp.concat(x));
        array.splice(i, 0, x);
      }
    }
    let result = [];
    p(array, []);
    return result;
  }

  getCopy(objectValue) {
    return JSON.parse(JSON.stringify(objectValue));
  }

  readData(fileContent) {
    let dataArray = fileContent.replace(/\r/g, '').split(' ');
    this.totalRows = dataArray[0];
    this.totalColumns = dataArray[1];
    console.log(`all`, this.totalRows * this.totalColumns);
    this.minIngredients = dataArray[2];
    let i = dataArray[3].indexOf('\n');
    this.maxSize = dataArray[3].substring(0, i);
    return this.convertToArray(dataArray[3].substring(i + 1));
  }

  convertToArray(string) {
    let array = [];
    array = string.split(/\n/).filter(item => !!item);
    for (let i = 0; i < array.length; i++) {
      array[i] = array[i].split('');
    }
    return array;
  }

  getTop(x, y, pizzaData) {
    let dot = {
      ...this.emptyDot
    };
    if (pizzaData[x - 1] && pizzaData[x - 1][y]) {
      dot.x = x - 1;
      dot.y = y;
      dot.value = pizzaData[x - 1][y];
    }
    return dot;
  }

  getBottom(x, y, pizzaData) {
    let dot = {
      ...this.emptyDot
    };
    if (pizzaData[x + 1] && pizzaData[x + 1][y]) {
      dot.x = x + 1;
      dot.y = y;
      dot.value = pizzaData[x + 1][y];
    }
    return dot;
  }

  getRight(x, y, pizzaData) {
    let dot = {
      ...this.emptyDot
    };
    if (pizzaData[x] && pizzaData[x][y + 1]) {
      dot.x = x;
      dot.y = y + 1;
      dot.value = pizzaData[x][y + 1];
    }
    return dot;
  }

  getLeft(x, y, pizzaData) {
    let dot = {
      ...this.emptyDot
    };
    if (pizzaData[x] && pizzaData[x][y - 1]) {
      dot.x = x;
      dot.y = y - 1;
      dot.value = pizzaData[x][y - 1];
    }
    return dot;
  }

  getNeighbors(x, y, pizzaData) {
    return {
      bottom: this.getBottom(x, y, pizzaData),
      right: this.getRight(x, y, pizzaData),
      left: this.getLeft(x, y, pizzaData),
      top: this.getTop(x, y, pizzaData)
    }
  }

  isString(value) {
    return typeof value === 'string';
  }

  isObject(value) {
    return typeof value === 'object';
  }

  joinLoop(params) {
    for (let x = 0; x < params.pizzaData.length; x++) {
      for (let y = 0; y < params.pizzaData[x].length; y++) {
        this.joinIteration(x, y, params);
      }
    }
  }

  joinIteration(x, y, {
    slices,
    pizzaData,
    useSlicesMerge = false,
    directions
  }) {
    let value = pizzaData[x][y];
    let neighbors = this.getNeighbors(x, y, pizzaData);
    if (this.isString(value)) {
      for (let direction of directions) {
        if (this.isString(neighbors[direction].value)) {
          if (value !== neighbors[direction].value) {
            this.createSlice(
              [value, neighbors[direction].value],
              [neighbors[direction].x, neighbors[direction].y],
              [x, y],
              slices,
              pizzaData,
            );
            return;
          }
        } else if (this.isObject(neighbors[direction].value)) {
          let dataForExpand = this.getDataForExpand(x, y, neighbors[direction].value, pizzaData);
          if (dataForExpand) {
            this.expandSlice(neighbors[direction].value, dataForExpand, pizzaData);
            return;
          }
        }
      };
    } else if (useSlicesMerge && this.isObject(value)) {
      for (let direction of directions) {
        if (this.isObject(neighbors[direction].value)) {
          let dataForMerge = this.getDataForMerge(value, neighbors[direction].value, pizzaData);
          if (dataForMerge) {
            this.mergeSlices(value, neighbors[direction].value, dataForMerge, slices,
              pizzaData);
            return;
          }
        }
      };
    }
  }

  createSlice(ingredients, point1, point2, slices, pizzaData) {
    let startPoint, endPoint;
    if ((point1[0] + point1[1]) < (point2[0] + point2[1])) {
      startPoint = point1;
      endPoint = point2;
    } else {
      startPoint = point2;
      endPoint = point1;
    }
    let slice = {
      total: {
        T: ingredients.filter(item => item === 'T').length,
        M: ingredients.filter(item => item === 'M').length
      },
      startPoint,
      endPoint,
    };
    pizzaData[point1[0]][point1[1]] = slice;
    pizzaData[point2[0]][point2[1]] = slice;
    slices.push(slice);
    this.rerunJoinLoop = true;
  }

  expandSlice(slice, data, pizzaData) {
    slice.total.T = data.total.T;
    slice.total.M = data.total.M;
    slice.startPoint = data.startPoint;
    slice.endPoint = data.endPoint;
    for (let x = slice.startPoint[0]; x <= slice.endPoint[0]; x++) {
      for (let y = slice.startPoint[1]; y <= slice.endPoint[1]; y++) {
        pizzaData[x][y] = slice;
      }
    }
    this.rerunJoinLoop = true;
  }

  mergeSlices(slice1, slice2, data, slices, pizzaData) {
    slice1.total.T = data.total.T;
    slice1.total.M = data.total.M;
    slice1.startPoint = data.startPoint;
    slice1.endPoint = data.endPoint;
    for (let x = slice1.startPoint[0]; x <= slice1.endPoint[0]; x++) {
      for (let y = slice1.startPoint[1]; y <= slice1.endPoint[1]; y++) {
        pizzaData[x][y] = slice1;
      }
    }
    slices.splice(slices.indexOf(slice2), 1);
    this.rerunJoinLoop = true;
  }

  suitsMaxSize(startX, startY, endX, endY) {
    if (((endX - startX + 1) * (endY - startY + 1)) <= this.maxSize) {
      return true;
    }
    return false;
  }

  isSliceValid(startX, startY, endX, endY, slices, pizzaData) {
    for (let x = startX; x <= endX; x++) {
      for (let y = startY; y <= endY; y++) {
        if (!pizzaData[x]) {
          return false;
        }
        let value = pizzaData[x][y];
        if (!value) {
          return false;
        }
        if (this.isObject(value)) {
          if (!slices.includes(value)) {
            return false;
          }
        }
      }
    }
    return true;
  }

  isSliceHaveCorrectTotal(startX, startY, endX, endY, slices, pizzaData) {
    let total = {
      M: slices.reduce((acc, b) => {
        return acc + b.total.M;
      }, 0),
      T: slices.reduce((acc, b) => {
        return acc + b.total.T;
      }, 0),
    };
    for (let x = startX; x <= endX; x++) {
      for (let y = startY; y <= endY; y++) {
        let value = pizzaData[x][y];
        if (this.isString(value)) {
          total[value]++;
        }
      }
    }
    return this.isSliceTotalCompleted(total);
  }

  getDataForExpand(x, y, slice, pizzaData) {
    let allX = [x, slice.startPoint[0], slice.endPoint[0]];
    let allY = [y, slice.startPoint[1], slice.endPoint[1]];
    let startX = Math.min.apply(Math, allX);
    let startY = Math.min.apply(Math, allY);
    let endX = Math.max.apply(Math, allX);
    let endY = Math.max.apply(Math, allY);

    let helperFlag = true,
      tempEndX = endX,
      tempEndY = endY;
    while (this.suitsMaxSize(startX, startY, tempEndX, tempEndY) &&
      this.isSliceValid(startX, startY, tempEndX, tempEndY, [slice], pizzaData)) {
      if (this.isSliceHaveCorrectTotal(startX, startY, tempEndX, tempEndY, [slice], pizzaData)) {
        endX = tempEndX;
        endY = tempEndY;
      }
      if (helperFlag) {
        tempEndX++;
      } else {
        tempEndY++;
      }
      helperFlag = !helperFlag;
    }
    if (!this.suitsMaxSize(startX, startY, endX, endY)) {
      return false;
    }

    let total = {
      ...slice.total
    };
    for (let x = startX; x <= endX; x++) {
      for (let y = startY; y <= endY; y++) {
        let value = pizzaData[x][y];
        if (this.isString(value)) {
          total[value]++;
        } else if (this.isObject(value)) {
          if (value !== slice) {
            return false;
          }
        }
      }
    }
    return {
      startPoint: [startX, startY],
      endPoint: [endX, endY],
      total,
    };
  }

  getDataForMerge(slice1, slice2, pizzaData) {
    if (slice1 === slice2) {
      return;
    }
    let allX = [slice1.startPoint[0], slice1.endPoint[0], slice2.startPoint[0], slice2.endPoint[0]];
    let allY = [slice1.startPoint[1], slice1.endPoint[1], slice2.startPoint[1], slice2.endPoint[1]];
    let startX = Math.min.apply(Math, allX);
    let startY = Math.min.apply(Math, allY);
    let endX = Math.max.apply(Math, allX);
    let endY = Math.max.apply(Math, allY);

    let helperFlag = true,
      tempEndX = endX,
      tempEndY = endY;
    while (this.suitsMaxSize(startX, startY, tempEndX, tempEndY) &&
      this.isSliceValid(startX, startY, tempEndX, tempEndY, [slice1, slice2], pizzaData)) {
      if (this.isSliceHaveCorrectTotal(startX, startY, tempEndX, tempEndY, [slice1, slice2], pizzaData)) {
        endX = tempEndX;
        endY = tempEndY;
      }
      if (helperFlag) {
        tempEndX++;
      } else {
        tempEndY++;
      }
      helperFlag = !helperFlag;
    }
    if (!this.suitsMaxSize(startX, startY, endX, endY)) {
      return false;
    }

    let total = {
      M: slice1.total.M + slice2.total.M,
      T: slice1.total.T + slice2.total.T
    };
    for (let x = startX; x <= endX; x++) {
      for (let y = startY; y <= endY; y++) {
        let value = pizzaData[x][y];
        if (this.isString(value)) {
          total[value]++;
        } else if (this.isObject(value)) {
          if (value !== slice1 && value !== slice2) {
            return false;
          }
        }
      }
    }
    return {
      startPoint: [startX, startY],
      endPoint: [endX, endY],
      total,
    };
  }

  getOutputData(slices) {
    let data = `${slices.length}`;
    slices.forEach(slice => {
      data += `\n${slice.startPoint[0]} ${slice.startPoint[1]} ${slice.endPoint[0]} ${slice.endPoint[1]}`;
    });
    return data;
  }

  isSliceTotalCompleted(total) {
    return total.T >= this.minIngredients && total.M >= this.minIngredients;
  }
}


let pizza = new Pizza();

pizza.cut('data/a_example.in', 'a_example_result.txt');
pizza.cut('data/b_small.in', 'b_small_result.txt');
pizza.cut('data/c_medium.in', 'c_medium_result.txt');
pizza.cut('data/d_big.in', 'd_big_result.txt');
